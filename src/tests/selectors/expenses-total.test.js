import getTotalExpense from '../../selectors/expenses-total'
import expenses from '../fixtures/expenses'
test('should return if no expenses ', () => {

    const total = getTotalExpense([]);
    expect(total).toBe(0);

})

test('should correclty add if  a single expense is passed', () => {
    const amount = expenses[0].amount;
    const total = getTotalExpense([expenses[0]])
    expect(total).toBe(amount);
})

test('should correclty add all if multiple expenses are passed', () => {
    const amountTotal = expenses[0].amount + expenses[1].amount + expenses[2].amount;
    const total = getTotalExpense(expenses)
    expect(total).toBe(amountTotal);
})