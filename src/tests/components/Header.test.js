import React from 'react';
import { shallow } from 'enzyme'
import { Header } from '../../components/Header'
import { startLogin } from '../../actions/auth';


test('should render Header correctyw', () => {
    const wrapper = shallow(<Header startLogout={() => { }} />);
    expect((wrapper)).toMatchSnapshot();

})

//should call start Logout on button click

test('should call start logout on button click ', () => {
    const startLogout = jest.fn();
    const wrapper = shallow(<Header startLogout={startLogout} />)
    wrapper.find('button').simulate('click');
    expect(startLogout).toHaveBeenCalled();

})

