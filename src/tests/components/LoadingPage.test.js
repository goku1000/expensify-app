import React from 'react';
import { shallow } from 'enzyme'
import LoadingPge, { LoadingPage } from '../../components/LoadingPage'

test('should render Loading page correctly', () => {
    const wrapper = shallow(<LoadingPge />);
    expect(wrapper).toMatchSnapshot();
})