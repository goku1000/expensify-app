import React from 'react'
import { shallow } from 'enzyme'
import ExpenseListItem from '../../components/ExpenseListItem'
import expenses from '../fixtures/expenses'

test('should render  Exepnse List Items with expense', () => {
    const wrapper = shallow(<ExpenseListItem {...expenses[0]} />);
    expect(wrapper).toMatchSnapshot();
})



test('should render  NO Expense List Item with no expense', () => {
    const wrapper = shallow(<ExpenseListItem />);
    expect(wrapper).toMatchSnapshot();
})