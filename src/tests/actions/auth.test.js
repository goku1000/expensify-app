import React from 'react'

import { login, logout } from '../../actions/auth'

test('should dipatch action for login', () => {
    const uid = '101';
    const action = login(uid);
    expect(action).toEqual({
        type: 'LOGIN',
        uid
    })
})


test('should dipatch action for logout', () => {
    const uid = '101';
    const action = logout(uid);
    expect(action).toEqual({
        type: 'LOGOUT',
        uid
    })
})