import React from 'react'
import authReducer from '../../reducers/auth'


test('should set default state', () => {
    const state = authReducer({}, { type: '@@INIT' })
    expect(state).toEqual({});
})

test('should login by uid', () => {
    const uid = 101;
    const action = {
        type: 'LOGIN',
        uid
    }
    const state = authReducer({}, action);
    expect(state.uid).toBe(action.uid)
})


test('should logout ', () => {
    const uid = 101;
    const action = {
        type: 'LOGOUT',
    }
    const state = authReducer({ uid: uid }, action);
    expect(state).toEqual({})
})

