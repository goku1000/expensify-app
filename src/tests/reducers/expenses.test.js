import expensesReducer from '../../reducers/expenses'
import expenses from '../fixtures/expenses'


test('should set default state', () => {
    const state = expensesReducer(undefined, { type: '@@INIT' })
    expect(state).toEqual([]);
})


test('should remove expense by Id', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: expenses[1].id
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[2]]);
})


test('should not remove expenses if Id is not found', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: '-1'
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
})

//should add an expense 

test('should add an expense ', () => {
    const expense = {
        id: '4',
        description: 'Tea',
        expenseNote: '',
        amount: 2000,
        createdAt: 25000000
    }

    const action = {
        type: 'ADD_EXPENSE',
        expense
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([...expenses, expense]);
})

//should edit an expnse 

test('should edit and expense', () => {
    const amount = 122000
    const action = {
        type: 'EDIT_EXPENSE',
        id: expenses[1].id,
        updates: {
            amount
        }
    }
    const state = expensesReducer(expenses, action)
    expect(state[1].amount).toEqual(amount)


})

//should edit not expense if epense not found

test('should not edit and expense', () => {
    const amount = 122000
    const action = {
        type: 'EDIT_EXPENSE',
        id: '-1',
        updates: {
            amount
        }
    }
    const state = expensesReducer(expenses, action)
    expect(state).toEqual(expenses)
})

test('should set expenses', () => {
    const expenses = [{
        id: '4',
        description: 'Chennai Rent',
        expenseNote: '',
        amount: 2000,
        createdAt: 25000000
    },
    {
        id: '5',
        description: 'Asansol Rent',
        expenseNote: '',
        amount: 2000,
        createdAt: 25000000
    }]

    const action = {
        type: 'SET_EXPENSES',
        expenses
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);

})

