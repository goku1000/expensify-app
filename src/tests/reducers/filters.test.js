import filtersReducers from '../../reducers/filters'
import moment from 'moment';

test('should setup default filter value', () => {

    const state = filtersReducers(undefined, { type: '@@INIT' });
    expect(state).toEqual({
        text: '',
        sortBy: 'date',
        startDate: moment(0),
        endDate: moment().endOf('month')
    })
})


test('should set sortBy to amount', () => {
    const state = filtersReducers(undefined, { type: 'SORT_BY_AMOUNT' })
    expect(state.sortBy).toBe('amount');
})

test('should set sortBy date', () => {
    const currentState = {
        text: '',
        startDate: undefined,
        endDate: undefined,
        sortBy: 'amount'
    }
    const action = { type: 'SORT_BY_DATE' }
    const state = filtersReducers(currentState, action);
    expect(state.sortBy).toBe('date');
})



test('should set text filter', () => {
    const action = { type: 'SET_TEXT_FILTER', text: 'e' };
    const state = filtersReducers(undefined, action);
    expect(state.text).toBe('e')
})

test('should set start date filter', () => {
    const startDate = moment(0);
    const action = {
        type: 'SET_START_DATE',
        startDate
    };
    const state = filtersReducers(undefined, action);
    expect(state.startDate).toEqual(startDate)
})

test('should set end date filter', () => {
    const endDate = moment();
    const action = {
        type: 'SET_END_DATE',
        endDate
    };
    const state = filtersReducers(undefined, action);
    expect(state.endDate).toEqual(endDate)
})  