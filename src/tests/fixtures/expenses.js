import moment from 'moment'
export default [{
    id: '1',
    description: 'gum',
    expenseNote: 'Eating GUM',
    amount: 195,
    createdAt: 0
},
{
    id: '2',
    description: 'Rent',
    expenseNote: '',
    amount: 195000,
    createdAt: moment(0).subtract(4, 'days').valueOf()
}, {
    id: '3',
    description: 'Coffee',
    expenseNote: '',
    amount: 45000,
    createdAt: moment(0).add(4, 'days').valueOf()
}]