//Higher Order Component (HOC) --A componentthat renders another component
//Reuse Code
//Render hiJacking
//Abstract State


import React from 'react';
import ReactDOM from 'react-dom';
const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The info is {props.info}</p>
    </div>
)


const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin && <p>This is privateInfo ,Please do not share</p>}
            <WrappedComponent {...props} />
        </div>
    );
};

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAuthenticated ? (
                <WrappedComponent {...props} />) : (
                    <p>Please Login In to view the info</p>
                )}
        </div >
    )
}

const AdminInfo = withAdminWarning(Info);


//requireAuthentication

const AuthInfo = requireAuthentication(Info)

/* 
ReactDOM.render(<AdminInfo isAdmin={false} info="There are the details" />,
    document.querySelector('#app'));
 */

ReactDOM.render(<AuthInfo isAuthenticated={true} info="there are the details" />,
    document.querySelector('#app'))