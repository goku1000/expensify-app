

/* -----------------OBJECT DESTRUCTURING --------------------------
const person = {
    name: 'suman',
    age: 22,
    location: {
        city: 'Asansol',
        temp: 95
    }
}

const { name: firstname = 'Anonymous', age } = person;

console.log(`${name} is ${age}`);


const { city, temp: temperature } = person.location;

if (city && temperature) {
    console.log(`it's ${temperature} in ${city}`)

}



const book = {
    title: 'Ego is the Enemy',
    author: 'Ryan Holiday',
    publisher: {
        name: 'Pengiun'
    }
};


const { name: publisherName = 'Self Published' } = book.publisher

console.log(publisherName);
*/


//------------------Array Destructuring----------------------------------------------------------------------------------------------

const address = ['Hill View South', 'Asansol', 'west bengal', '2020']


const [, city, state = 'Bihar'] = address;

console.log(`Your are in ${state} `)
const item = ['Coffee(hot)', '$2.00', '$2.50', '$2.75'];


const [coffee, , price] = item;
console.log(` A medium ${coffee} cost ${price}`);