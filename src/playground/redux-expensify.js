import { createStore, combineReducers } from 'redux';
import { v4 as uuidv4 } from 'uuid';

//ADD_EXPENSE

const addExpense = ({
    description = '',
    expenseNote = '',
    amount = 0,
    createdAt = 0
} = {}) => ({
    type: 'ADD_EXPENSE',
    expense: {
        id: uuidv4(),
        description,
        expenseNote,
        amount,
        createdAt
    }

});

//REMOVE_EXPENSE
const removeExpense = ({ id } = {}) => ({
    type: 'REMOVE_EXPENSE',
    id
});

//EDIT_EXPENSE

const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
});
//SET_TEXT_FILTER

const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text

});

//SORT_BY_DATE
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});
//SORT_BY_AMOUNT
const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
});

//SET_START_DATE
const setStartDate = (date = undefined) => ({
    type: 'SET_START_DATE',
    date
});
//SET_END_DATE

const setEndDate = (date = undefined) => ({
    type: 'SET_END_DATE',
    date
});

//Expenses Reducer

const expensesReducerDefaultState = [];

const expensesReducer = (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [
                ...state,
                action.expense
            ]
        case 'REMOVE_EXPENSE':
            return state.filter(({ id }) => id != action.id)
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                if (expense.id === action.id) {
                    return {
                        ...expense,
                        ...action.updates
                    }
                } else {
                    return expense;
                }
            })
        default:
            return state;
    }

}

//FilterReducer

const filterReducerDefaultState = {
    text: '',
    sortBy: 'date',
    stateDate: undefined,
    endDate: undefined
};
const FilterReducer = (state = filterReducerDefaultState, action) => {

    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text
            }
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            }
        case 'SORT_BY_AMOUNT':
            return {
                ...state,
                sortBy: 'amount'
            }
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.date
            }
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.date
            }
        default:
            return state
    }
}
//Get Visible expenses
const getVisibleExpens = (expenses, { text, sortBy, startDate, endDate }) => {
    return expenses.filter(expense => {
        const forStartDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof startDate !== 'number' || expense.createdAt <= endDate;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());
        return forStartDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if (sortBy === 'date') {
            return a.createdAt < b.createdAt ? 1 : -1
        }
        if (sortBy === 'amount') {
            return a.amount < b.amount ? 1 : -1
        }
    });
}


//Store Creation

const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: FilterReducer

    })
);

store.subscribe(() => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpens(state.expenses, state.filters)
    console.log(visibleExpenses);
});
const expense1 = store.dispatch(addExpense({ description: 'Rent', amount: 100, createdAt: -2000 }));
const expense2 = store.dispatch(addExpense({ description: 'Coffee', amount: 300, createdAt: -1008 }));

/*
store.dispatch(removeExpense({ id: expense1.expense.id }));

store.dispatch(editExpense(expense2.expense.id, { amount: 500 }));


store.dispatch(setTextFilter(''));

store.dispatch(sortByAmount()); //amount
store.dispatch(sortByDate());   //date
 */
//store.dispatch(setStartDate(125));
/* store.dispatch(setStartDate());


store.dispatch(setEndDate(1000));
store.dispatch(setEndDate());
 */
//store.dispatch(setTextFilter('fEe'));

const demoState = {
    expenses: [{
        id: "1010101",
        description: 'January Rent',
        expenseNote: 'This was the final payment for that address',
        amount: 54500,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount',//date or amount
        startDate: undefined,
        endDate: undefined
    }
}

