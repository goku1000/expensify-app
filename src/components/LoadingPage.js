import React from 'react';

export const LoadingPge = () => (
    <div className="loader">
        <img className="loader__image" src="/images/loader.gif" />
    </div>
)
export default LoadingPge;