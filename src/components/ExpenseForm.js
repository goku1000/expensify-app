import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';

export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.expense ? props.expense.description : '',
            expenseNote: props.expense ? props.expense.expenseNote : '',
            amount: props.expense ? (props.expense.amount / 100).toString() : '',
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calenderFocused: false,
            error: undefined
        };
    }

    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    }
    onExpenseNoteChange = (e) => {
        const expenseNote = e.target.value;
        this.setState(() => ({ expenseNote }));
    }

    onAmountChange = (e) => {
        const amount = e.target.value;
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }))
        }

    }

    onDateChange = (createdAt) => {
        if (createdAt) {
            this.setState(() => ({ createdAt }));
        }
    }

    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calenderFocused: focused }));
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.state.description || !this.state.amount) {
            //Set error State equal to 'Please provide description and amount'
            this.setState(({ error: 'Description or Amount is not entered' }))
        }
        else {
            //Clear the error 
            this.setState(({ error: undefined }))
            this.props.onSubmit({
                description: this.state.description,
                expenseNote: this.state.expenseNote,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf()
            });
        }
    };

    render() {
        return (

            <form className="form" onSubmit={this.onSubmit}>
                <div>{this.state.error && <p className="form__error">{this.state.error}</p>}</div>
                <input
                    className="text-input"
                    type="text"
                    placeholder="Description"
                    autoFocus
                    value={this.state.description}
                    onChange={this.onDescriptionChange}
                />
                <input
                    type="text"
                    className="text-input"
                    placeholder="Amount"
                    value={this.state.amount}
                    onChange={this.onAmountChange}
                />
                <SingleDatePicker
                    date={this.state.createdAt}
                    onDateChange={this.onDateChange}
                    focused={this.state.calenderFocused}
                    onFocusChange={this.onFocusChange}
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                />
                <textarea
                    className="textarea"
                    placeholder="Add a expenseNote for your expense(optional!)"
                    value={this.state.expenseNote}
                    onChange={this.onExpenseNoteChange}
                >
                </textarea>
                <div>
                    <button className="button" > Save Expense</button>
                </div>

            </form>

        )
    }
}