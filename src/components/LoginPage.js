import React from 'react';
import { connect } from 'react-redux'
import { startLogin } from '../actions/auth'

export const LoginPage = ({ startLogin }) => {
    return (
        <div className="box-layout">
            <div className="box-layout__box">
                <h1 className="box-layout__title">Expensify... </h1>
                <p>It's time to get your expenses sorted</p>
                <button onClick={startLogin} class="button_login">
                    Login With Google Account
                    <div class="fa fa-google"></div>

                </button>
            </div>
        </div>
    )
}

const mapDispathToProps = (dispatch) => ({
    startLogin: () => dispatch(startLogin())
})

export default connect(undefined, mapDispathToProps)(LoginPage)