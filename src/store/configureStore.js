import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk'
import expensesReducer from '../reducers/expenses'
import FilterReducer from '../reducers/filters'

import authReducer from '../reducers/auth'

const componseEnhancers = window.__REDUX_DEVTOOLS_EXTENSION__COMPOSE__ || compose;
export default () => {

    const store = createStore(
        combineReducers({
            expenses: expensesReducer,
            filters: FilterReducer,
            auth: authReducer

        }),
        componseEnhancers(applyMiddleware(thunk))

        //   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
}


