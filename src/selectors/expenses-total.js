import expenses from "../tests/fixtures/expenses";

let getTotalExpense = (expenses = {}) => {
    let total = 0;
    if (expenses) {
        expenses.map(expense => {
            if (expense.amount !== 0 || expense.amount !== undefined || expense.amount !== '')
                total = expense.amount + total;

        });

    }
    return total;
}


/* let getTotalExpense = (expense) => {
    if (expenses.length === 0) {
        return 0;
    }
    else {
        return expenses
            .map(expense => expense.amount)
            .reduce((sum, value) =>  sum + value)
    }
}
 */

export default getTotalExpense;